# Trabajo javascript 
_Ejercicios Metodos Constructores-Objetos-Clases_

### Integrantes grupos
- __Jhonathan Grisales Giraldo__
##

### EJERCICIO 1 -Crear un metodo constructor llamada persona. Sus atributos son: nombre, edad y cedula.
___

```js
function persona(nombre, edad, cedula) {

    this.nombre = nombre;
    this.edad = edad;
    this.cedula = cedula;
}

let personaUno = new persona("Jhonathan", 17, 1060650078);

```

### EJERCICIO 1.1 -- Mostrar datos

```js
function persona(nombre, edad, cedula) {

    this.nombre = nombre;
    this.edad = edad;
    this.cedula = cedula;

    this.mostrarDatos = function () {

        return `Persona =  Nombre: ${this.nombre}, Edad:  ${this.edad},  Cedula:  ${this.cedula}`

    }
}

    let personaUno = new persona("Jhonathan", 17, 1060650078);
    personaUno.mostrarDatos();

 ```

 ![Alt text](./img/1.jpg?raw=true "Title")

 
### EJERCICIO 1.2 -- ¿Es mayor de edad?

```js

function persona(nombre, edad, cedula) {

    this.nombre = nombre;
    this.edad = edad;
    this.cedula = cedula;

     this.mayor = function (persona) {

        if (persona.edad >= 18) {

            console.log(persona.nombre + ' es mayor de edad')
            return true
        } else {

            console.log(persona.nombre + ' no es mayor de edad')
            return false
        }
    }

}

let personaUno = new persona("Jhonathan", 17, 1060650078);
let personaDos = new persona("Camilo", 35, 1060650078);
personaUno.mayor(personaDos);

 ```

 ![Alt text](./img/2.jpg?raw=true "Title")

  ![Alt text](./img/3.jpg?raw=true "Title")


 ### EJERCICIO 2 -Crea un metodo constructor llamado cuenta que tendrá los siguientes atributos: titular (que es nombre de la persona) y cantidad. El titular será obligatorio y la cantidad es opcional.
 ___


```js


function cuenta() {

    this.nombre = '';
    this.cantidad = '';

    //el ciclo while hace que el ususrio introduzca los dos datos necesarios para crear la cuenta

    this.crear = function () {

        while (this.nombre == '' || this.cantidad == '' || this.nombre == null || this.cantidad == null) {

            this.nombre = prompt('Ingrese Nombre Titular');
            this.cantidad = prompt('Ingrese cantidad inicial')

        }

        this.cantidad = parseFloat(this.cantidad);
    }

}

let cuentaUno = new cuenta();
cuentaUno.crear();

 ```


 ### EJERCICIO 2.1 -- Mostrar datos

```js

 function cuenta() {

    this.nombre = '';
    this.cantidad = '';

    this.crear = function () {

        while (this.nombre == '' || this.cantidad == '' || this.nombre == null || this.cantidad == null) {

            this.nombre = prompt('Ingrese Nombre Titular');
            this.cantidad = prompt('Ingrese cantidad inicial')

        }

        this.cantidad = parseFloat(this.cantidad);
    }, this.mostrar = function () {

        return console.log(' Nombre del titular: ' + this.nombre + " , Cantidad en cuenta: " + this.cantidad);

    }

}

let cuentaUno = new cuenta();
cuentaUno.crear();
cuentaUno.mostrar();

```


 ![Alt text](./img/4.jpg?raw=true "Title")

 ### EJERCICIO 2.2 -- Ingresar cantidad

 ```js

 function cuenta() {

    this.nombre = '';
    this.cantidad = '';

    this.crear = function () {

        while (this.nombre == '' || this.cantidad == '' || this.nombre == null || this.cantidad == null) {

            this.nombre = prompt('Ingrese Nombre Titular');
            this.cantidad = prompt('Ingrese cantidad inicial')

        }

        this.cantidad = parseFloat(this.cantidad);
    }, this.mostrar = function () {

        return console.log(' Nombre del titular: ' + this.nombre + " , Cantidad en cuenta: " + this.cantidad);

    },this.ingresarCantidad = function (cantidadCuenta) {

        if (cantidadCuenta >= 0) {

            this.cantidad = this.cantidad + cantidadCuenta;

        } else if (cantidadCuenta < 0) {

            this.cantidad = this.cantidad;

        }
    }

}
 

let cuentaUno = new cuenta();
cuentaUno.crear();
cuentaUno.mostrar();
cuentaUno.ingresarCantidad(2500);
cuentaUno.mostrar();

 ```

 ![Alt text](./img/5.jpg?raw=true "Title")


 ### EJERCICIO 2.3 -- Retirar Cantidad

  ```js

 function cuenta() {

    this.nombre = '';
    this.cantidad = '';

    this.crear = function () {

        while (this.nombre == '' || this.cantidad == '' || this.nombre == null || this.cantidad == null) {

            this.nombre = prompt('Ingrese Nombre Titular');
            this.cantidad = prompt('Ingrese cantidad inicial')

        }

        this.cantidad = parseFloat(this.cantidad);

    }, this.mostrar = function () {

        return console.log(' Nombre del titular: ' + this.nombre + " , Cantidad en cuenta: " + this.cantidad);

    }, this.ingresarCantidad = function (cantidadCuenta) {

        if (cantidadCuenta >= 0) {

            this.cantidad = this.cantidad + cantidadCuenta;

        } else if (cantidadCuenta < 0) {

            this.cantidad = this.cantidad;

        }


    }, this.retirarCantidad = function (retiro) {

        if (retiro >= 0) {

            this.cantidad = this.cantidad - retiro;
        }

    }

}


let cuentaUno = new cuenta();
cuentaUno.crear();
cuentaUno.mostrar();
cuentaUno.ingresarCantidad(2500);
cuentaUno.mostrar();
cuentaUno.retirarCantidad(1500);
cuentaUno.mostrar();

  ```

  ![Alt text](./img/6.jpg?raw=true "Title")

  
### EJERCICIO 3 -Crear un metodo constructor llamado formulas.

  ```js

function formulas() {

    this.sumar = function (enteroUno, enteroDos) {

    }

    let formula = new formulas();

```

 ### EJERCICIO 3.1 -- Sumar(entero, entero)

 ```js

 function formulas() {

    this.sumar = function (enteroUno, enteroDos) {

        return `${enteroUno + enteroDos}`

    }
 }

 let formula = new formulas();
formula.sumar(4,7);

```

![Alt text](./img/7.jpg?raw=true "Title")

### EJERCICIO 3.2 --  Fibonacci(cantidad)

```js

function formulas() {
        
     this.fibonacci = function (cantidad) {

        let numeros = [0, 1];

        for (let i = 2; i < cantidad; i++) {

            numeros[i] = numeros[i - 2] + numeros[i - 1];
        }
        return numeros;

    }
}

let formula = new formulas();
formula.fibonacci(10);

```

![Alt text](./img/8.jpg?raw=true "Title")


### EJERCICIO 3.3 --  Operacion modulo ingresando una cantidad tope (divisor estipulado = 2)

```js

function formulas() {

    this.sumar = function (enteroUno, enteroDos) {

        return `${enteroUno + enteroDos}`

    }, this.fibonacci = function (cantidad) {

        let numeros = [0, 1];

        for (let i = 2; i < cantidad; i++) {

            numeros[i] = numeros[i - 2] + numeros[i - 1];
        }
        return numeros;

    }, this.operacionModulo = function (cantidad) {

        let elemento = [0];
        let contador = 0; 

        for (let index = 1; index <= cantidad; index++) {


            if (index % 2 == 0) {      

                  elemento[contador]= index ;
                  contador++;
                  
            }

        }

        console.log('Los siguientes numeros al dividirlos por 2 dan residuo 0')
        return elemento

    }
}

let formula = new formulas();
formula.operacionModulo(10);

```

![Alt text](./img/9.jpg?raw=true "Title")


## EJERCICIO 3.4 --  Sacar primos hasta una cantidad tope. 

```js

function formulas() {

    this.numerosPrimos = function (primos) {

        for (let x = 2; x <= primos; x++) {

            let primo = true;
            let div = 2;

            while (primo && div < x) {

                if (x % div == 0) {

                    primo = false

                } else {

                    div = div + 1;
                }
            }
            if (primo == true)

                console.log(x)
        }

    }
}

let formula = new formulas();
formula.numerosPrimos(20);

```

![Alt text](./img/10.jpg?raw=true "Title")



 ### EJERCICIO 4 -Crear un metodo constructor llamado persona. Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura. 

```js

 function personaIMC(nombre, edad, DNI, sexo, peso, altura) {

    this.nombre = nombre;
    this.edad = edad;
    this.DNI = DNI;
    this.sexo = sexo;
    this.peso = peso;
    this.altura = altura;

 }

 let personaDos = new personaIMC("Camilo", 45, 1060650078, 'M', 102, 1.82);
  
```
## EJERCICIO 4.1 -- CalcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2 en m))

```js

function personaIMC(nombre, edad, DNI, sexo, peso, altura) {

    this.nombre = nombre;
    this.edad = edad;
    this.DNI = DNI;
    this.sexo = sexo;
    this.peso = peso;
    this.altura = altura;

    this.pesoIdeal = function (peso, altura) {


        let IMC = (peso / (altura ** 2));

        if (IMC < 20) {

            return console.log('El indice es de: ' + -1);

        } else if (IMC >= 20 && IMC <= 25) {

            return console.log('El indice es de: ' + 0);

        } else if (IMC > 25) {

            return console.log('El indice es de: ' + 1);
        }
    }

}

let personaDos = new personaIMC("Camilo", 45, 1060650078, 'M', 102, 1.82);
personaDos.pesoIdeal(personaDos.peso, personaDos.altura);
  
```
![Alt text](./img/11.jpg?raw=true "Title")


## EJERCICIO 4.2 -- esMayorDeEdad(): indica si es mayor de edad. 

```js

function personaIMC(nombre, edad, DNI, sexo, peso, altura) {

    this.nombre = nombre;
    this.edad = edad;
    this.DNI = DNI;
    this.sexo = sexo;
    this.peso = peso;
    this.altura = altura;

    this.pesoIdeal = function (peso, altura) {


        let IMC = (peso / (altura ** 2));

        if (IMC < 20) {

            return -1;

        } else if (IMC >= 20 && IMC <= 25) {

            return 0;

        } else if (IMC > 25) {

            return 1;
        }


    }, this.mayorEdad = function (persona) {

        if (persona.edad >= 18) {

            console.log(persona.nombre + ' es mayor de edad')
            return true

        } else {

            console.log(persona.nombre + ' no es mayor de edad')
            return false
        }
    }
}


let personaDos = new personaIMC("Pedro Marin", 50, 25234230, 'M', 102, 1.82);
personaDos.mayorEdad(personaDos);

```

![Alt text](./img/12.jpg?raw=true "Title")


## EJERCICIO 4.3 -- ComprobarSexo(char sexo): comprueba que el sexo introducido es correcto. Si no es correcto, sera H.

```js

function personaIMC(nombre, edad, DNI, sexo, peso, altura) {

    this.nombre = nombre;
    this.edad = edad;
    this.DNI = DNI;
    this.sexo = sexo;
    this.peso = peso;
    this.altura = altura;

    this.pesoIdeal = function (peso, altura) {


        let IMC = (peso / (altura ** 2));

        if (IMC < 20) {

            return -1;

        } else if (IMC >= 20 && IMC <= 25) {

            return 0;

        } else if (IMC > 25) {

            return 1;
        }


    }, this.mayorEdad = function (persona) {

        if (persona.edad >= 18) {

            console.log(persona.nombre + ' es mayor de edad')
            return true

        } else {

            console.log(persona.nombre + ' no es mayor de edad')
            return false
        }

    }, this.comprobar = function (sexo) {

        if (sexo == 'M' || sexo == 'H') {

            console.log('Sexo correcto')
            return true

        } else {

            console.log('Sexo no correcto!!! se reestablece por el predeterminado H')
            this.sexo = 'H'
        }
    }
}

let personaTres = new personaIMC("Camilo", 45, 25234230, 'M', 102, 1.82);
personaDos.comprobar(personaDos.sexo);

let personaTres = new personaIMC("Marcelo", 17, 1060650078, 'X', 56, 1.65);
personaTres.comprobar(personaTres.sexo);


```
![Alt text](./img/13.jpg?raw=true "Title")


 ### EJERCICIO 5 -Crear un metodo constructor llamado contraseña. Sus atributos longitud y contraseña.

 ```js

function contraseña(longitud, contraseña) {

    this.longitud = longitud;
    this.contraseña = contraseña;
}

let contraseñaUno = new contraseña(12, '12345ABCab');

 ```

 ### EJERCICIO 5.1 -- esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe tener mas de 2 mayúsculas, mas de 1 minúscula y mas de 5 números.

```js
 
function contraseña(longitud, contraseña) {

    this.longitud = longitud;
    this.contraseña = contraseña;

    this.esFuerte = function (contraseñaComprobar) {


        var cuentanumeros = 0;
        var cuentaminusculas = 0;
        var cuentamayusculas = 0;
        
        //Recorriend contraseña y verificando el tipo de dato 
        for (let i = 0; i < contraseñaComprobar.contraseña.length; i++) {

            if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toLowerCase() && isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentaminusculas = cuentaminusculas + 1;

            } else if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toUpperCase() && isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentamayusculas = cuentamayusculas + 1;

            } else if (!isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentanumeros = cuentanumeros + 1;
            }
        }

        //Si la constraseña tiene mas de 5 numeros, mas de 1 minuscula y mas de 2 mayusculas
        if (cuentanumeros >= 5 && cuentaminusculas >= 1 && cuentamayusculas >= 2) {
            console.log('Es fuerte')
            return true;
        } else {

            console.log('No es fuerte')
            return false;
        }
    }
}

let contraseñaUno = new contraseña(12, '12345ABCab');
contraseñaUno.esFuerte(contraseñaUno);

let contraseñaTres = new contraseña(12, '123BCab');
contraseñaTres.esFuerte(contraseñaTres);

```
![Alt text](./img/14.jpg?raw=true "Title")

### EJERCICIO 5.2 -- generarPassword(): genera la contraseña del objeto con la longitud que tenga.

```js

function contraseña(longitud, contraseña) {

    this.longitud = longitud;
    this.contraseña = contraseña;

 this.generarPassword = function (longitud) {

        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;

        for (let i = 0; i < longitud; i++) {

            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        this.contraseña = result;
        return result;
 }

}

let contraseñaDos = new contraseña(12, '');
contraseñaDos.generarPassword(contraseñaDos.longitud);

```

![Alt text](./img/15.jpg?raw=true "Title")


### EJERCICIO 5.3 -- seguridadPaswword(); indicar si la contraseña es debil contiene entre 1 a 6 caracteres (caracteres numeros y letras), media (7 a 10 caracteres numeros y letras) o fuerte (11 a 20 caracteres letras y caracteres especiales)

```js

function contraseña(longitud, contraseña) {

    this.longitud = longitud;
    this.contraseña = contraseña;

this.seguridadPaswword = function (contraseñaComprobar) {

        let cuentanumeros = 0;
        let cuentaminusculas = 0;
        let cuentamayusculas = 0;
        let cunentaespeciales = 0;
        let letrasTotal = 0;


        const iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";

        for (var i = 0; i < contraseñaComprobar.contraseña.length; i++) {

            if (iChars.indexOf(contraseñaComprobar.contraseña.charAt(i)) != -1) {

                cunentaespeciales = cunentaespeciales + 1;

            }
        }

        //Recorriend contraseña y verificando el tipo de dato 
        for (var i = 0; i < contraseñaComprobar.contraseña.length; i++) {

            if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toLowerCase()
                && isNaN(contraseñaComprobar.contraseña.charAt(i)) && iChars.indexOf(contraseñaComprobar.contraseña.charAt(i)) == -1) {
                cuentaminusculas = cuentaminusculas + 1;

            } else if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toUpperCase()
                && isNaN(contraseñaComprobar.contraseña.charAt(i)) && iChars.indexOf(contraseñaComprobar.contraseña.charAt(i)) == -1) {
                cuentamayusculas = cuentamayusculas + 1;

            } else if (!isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentanumeros = cuentanumeros + 1;
            }
        }

        letrasTotal = cuentamayusculas + cuentaminusculas;


        //DEBIL
        if (cuentanumeros + letrasTotal <= 6) {

            return console.log('Contraseña Debil')
            //MEDIA
        } else if (cuentanumeros + letrasTotal >= 7 && cuentanumeros + letrasTotal <= 10) {

            return console.log('Contraseña Media')
            //ALTA  
        } else if (cunentaespeciales + letrasTotal + cuentanumeros >= 11 && cuentanumeros + letrasTotal + cunentaespeciales <= 20 && cunentaespeciales > 0) {

            return console.log('Contraseña Alta')

        } else if (cunentaespeciales + letrasTotal + cuentanumeros >= 11 && cuentanumeros + letrasTotal + cunentaespeciales <= 20 && cunentaespeciales == 0) {

            return console.log('La contraseña es Alta pero no contiene caracteres especiales')

        } else if (cunentaespeciales + letrasTotal + cuentanumeros > 20 || letrasTotal + cuentanumeros + cunentaespeciales > 20) {

             return console.log('Contraseña excede el numero de evaluacion soportada')

        }

    }

}

let contraseñaUno = new contraseña(12, '12345ABCab');
let contraseñaDos = new contraseña(12, '123456ujADV');
let contraseñaTres = new contraseña(12, '123456ujADV*/+');
let contraseñaCuatro = new contraseña(12, '125ABb');
contraseñaUno.seguridadPaswword(contraseñaUno);
contraseñaDos.seguridadPaswword(contraseñaDos);
contraseñaTres.seguridadPaswword(contraseñaTres);
contraseñaCuatro.seguridadPaswword(contraseñaCuatro);

```
![Alt text](./img/16.jpg?raw=true "Title")


### EJERCICIO 6 -Implementar un objeto que modele un contador. Un contador se puede incrementar o decrementar, recordando el valor actual. Al resetear un contador, se pone en cero. 

### Además es posible indicar directamente cual es el valor actual. Este objeto debe entender los siguientes mensajes: 6.1 reset() 6.2 inc() 6.3 dec() 6.4 valorActual() 6.5 valorActual(nuevoValor) P.ej. si se evalúa la siguiente secuencia contador.valorActual(10) contador.inc() contador.inc() contador.dec() contador.inc() contador.valorActual() el resultado debe ser 12.

```js

function Contador(ultimoComando) {

    this.reset = function () {
        this.numero = 0;

    },
        this.inc = function () {
            this.numero++

        },
        this.dec = function () {
            this.numero--

        },
        this.valorActual = function (valor) {


            if (valor != null) {

                this.numero = valor;

            } else {
                return this.numero;

            }
        }
}

//SE CREA EL OBJETO Y SE HACEN LOS PASOS (RESULTADO FINAL ES 12)

let contador = new Contador();
contador.reset();
contador.valorActual(10);
contador.inc();
contador.inc();
contador.dec();
contador.inc();
console.log('El Valor actual del contador es: ' + contador.valorActual());

```

![Alt text](./img/17.jpg?raw=true "Title")


### EJERCICIO 7 -Agregar al contador del ejercicio 6, la capacidad de recordar un String que representa el último comando que se le dio.

```js

function Contador(ultimoComando) {

    this.ultimoComan = ultimoComando;

    this.reset = function () {
        this.numero = 0;
        this.ultimoComan = 'reset'
    },
        this.inc = function () {
            this.numero++
            this.ultimoComan = 'Incremento'
        },
        this.dec = function () {
            this.numero--
            this.ultimoComan = 'Decremento'
        },
        this.valorActual = function (valor) {

            if (valor != null) {
                this.numero = valor;
                this.ultimoComan = 'Actualizacion'
            } else {
                return this.numero;

            }
        }, this.ultimoComando = function () {

            return this.ultimoComan;
        }
}

let contador = new Contador();
contador.reset();
contador.valorActual(10);
contador.inc();
contador.inc();
contador.dec();
contador.inc();
console.log('El Valor actual del contador es: ' + contador.valorActual());

contador.ultimoComando()

```

![Alt text](./img/18.jpg?raw=true "Title")



### EJERCICIO 8 -Implementar un objeto que modele a Chimuela, una dragona de la que nos interesa saber qué energía tiene en cada momento, medida en joules. 

```js

function Chimuela(energiaActual) {

    this.energiaActual = energiaActual;

    this.comer = function (gramos) {

        this.energiaActual = this.energiaActual + (gramos * 4);

    }, this.volar = function (kilometros) {

        this.energiaActual = this.energiaActual - (kilometros);
        this.energiaActual = this.energiaActual - 10;

    }, this.energia = function () {

        return this.energiaActual;

    }
}

let chimuela = new Chimuela(0);

chimuela.comer(100)
chimuela.volar(10);
chimuela.volar(20);
console.log ('la energia actual es de: ' + chimuela.energia() + ' joules' )

```

![Alt text](./img/19.jpg?raw=true "Title")


### EJERCICIO 9 -Agregar al metodo constructor de Chimuela del ejercicio 8, la capacidad de entender estos mensajes: estaDebil(), Chimuela está débil si su energía es menos de 50. estaFeliz(), Chimuela está feliz si su energía está entre 500 y 1000. cuantoQuiereVolar(), que es el resultado de la siguiente cuenta.


```js


function Chimuela(energiaActual) {

    this.energiaActual = energiaActual;

    this.comer = function (gramos) {

        this.energiaActual = this.energiaActual + (gramos * 4);

    }, this.volar = function (kilometros) {

        this.energiaActual = this.energiaActual - (kilometros);
        this.energiaActual = this.energiaActual - 10;

    }, this.energia = function () {

        return this.energiaActual;

    }, this.estadoEnergia = function (dragon) {

        if (dragon.energiaActual <= 50) {

            return console.log('Chimuelo esta debil')

        } else if (dragon.energiaActual >= 50 && dragon.energiaActual <= 1000) {

            return console.log('Chimuelo esta Feliz')
        }
    }, this.cuantoQuiereVolar = function () {

        var km = (this.energiaActual / 5)

        if (this.energiaActual < 300) {

            return console.log('chimuela quiere volar ' + km)

        } else if (this.energiaActual >= 300 && this.energiaActual <= 400 && this.energiaActual % 20 != 0) {

            km = km + 10;

            return console.log('chimuela quiere volar ' + km + ' kms')

        } else if (this.energiaActual >= 300 && this.energiaActual <= 400 && this.energiaActual % 20 == 0) {

            km = km + 10 + 15

            return console.log('chimuela quiere volar ' + km + ' kms')
        }

    }

}

let chimuela = new Chimuela(0);

chimuela.comer(85)
chimuela.volar(10);
chimuela.volar(20);
console.log ('la energia actual es de: ' + chimuela.energia() + ' joules' )

chimuela.estadoEnergia(chimuela);
chimuela.cuantoQuiereVolar();

```


![Alt text](./img/20.jpg?raw=true "Title")

### EJERCICIO 10 -Implementar un objeto que represente una calculadora sencilla, que permita sumar, restar y multiplicar.

```js

function Calculadora() {

    this.cargarNumero = function (numero) {

        this.cargar = numero;

    }, this.sumar = function (numero) {

        this.cargar = this.cargar + numero;

    }, this.restar = function (numero) {

        this.cargar = this.cargar - numero;

    }, this.multiplicar = function (numero) {

        this.cargar = this.cargar * numero;

    }, this.valorActuloperacion = function () {

        return this.cargar;

    }
}

let calculadora = new Calculadora();

calculadora.cargarNumero(0)
calculadora.sumar(4)
calculadora.multiplicar(5)
calculadora.restar(8)
calculadora.multiplicar(2)
console.log('El valor actual de la calculadora es: ' + calculadora.valorActuloperacion());

```

![Alt text](./img/21.jpg?raw=true "Title")


### EJERCICIO 11 -rear un metodo constructor llamado Libro. Sus atributos título del libro, autor, número de ejemplares del libro y número de ejemplares prestados los siguiente metodos para la clase:préstamo() que incremente el atributo correspondiente cada vez que se realice un préstamo del libro. No se podrán prestar libros de los que no queden ejemplares disponibles para prestar. Devuelve true si se ha podido realizar la operación y false en caso contrario. devolucion() que decremente el atributo correspondiente cuando se produzca la devolución de un libro. No se podrán devolver libros que no se hayan prestado. Devuelve true si se ha podido realizar la operación y false en caso contrario. toString() para mostrar los datos de los libros.

```js


function Libro(tituloLibro, autor, numeroEjemplares, numeroEjemplaresPrestado) {

    this.tituloLibro = tituloLibro;
    this.autor = autor;
    this.numeroEjemplares = numeroEjemplares;
    this.numeroEjemplaresPrestado = numeroEjemplaresPrestado;

    this.prestamo = function (Libro) {

        if (Libro.numeroEjemplaresPrestado < Libro.numeroEjemplares) {

            Libro.numeroEjemplaresPrestado++;
            console.log('Se presto el libro: ' + Libro.tituloLibro)
            return true;

        } else if (Libro.numeroEjemplaresPrestado == Libro.numeroEjemplares) {

            console.log('No hay ejemplares disponibles del libro:  ' + Libro.tituloLibro)
            return false;

        }
    }, this.devolucion = function (Libro) {

        if (Libro.numeroEjemplaresPrestado <= Libro.numeroEjemplares && Libro.numeroEjemplaresPrestado != 0) {

            Libro.numeroEjemplaresPrestado--;
            console.log('Se devolvio el libro : ' + Libro.tituloLibro)
            return true;

        } else if (Libro.numeroEjemplaresPrestado == 0) {

            console.log('El libro ' + Libro.tituloLibro + ' no esta prestado')
            return false;


        }

    }, this.toString = function () {

        return console.log('Titulo libro: ' + this.tituloLibro + ', Autor: ' + this.autor +
            ', Numero ejemplares: ' + this.numeroEjemplares + ', Numero ejemeplares prestado: ' + this.numeroEjemplaresPrestado)

    }
}

let libroUno = new Libro('Principito', 'Antonie', 5, 0);
let libroDos = new Libro('Nacho lee', 'Jorge Luis', 7, 0);
let libroTres = new Libro('Coquito clasico', 'Adrian Duf.', 3, 0);

libroUno.toString();

libroUno.prestamo(libroUno);
libroUno.prestamo(libroUno);
libroUno.prestamo(libroUno);
libroUno.prestamo(libroUno);

libroUno.devolucion(libroUno);
libroUno.devolucion(libroUno);
libroUno.devolucion(libroUno);
libroUno.devolucion(libroUno);

```

![Alt text](./img/22.jpg?raw=true "Title")


### EJERCICIO 12 -Se está pensando en el diseño de un juego que incluye la nave espacial Enterprise. En el juego, esta nave tiene un nivel de potencia de 0 a 100, y un nivel de coraza de 0 a 20. La Enterprise puede encontrarse con una pila atómica, en cuyo caso su potencia aumenta en 25. encontrarse con un escudo, en cuyo caso su nivel de coraza aumenta en 10. recibir un ataque, en este caso se especi�can los puntos de fuerza del ataque recibido. La Enterprise �para� el ataque con la coraza, y si la coraza no alcanza, el resto se descuenta de la potencia. P.ej. si la Enterprise con 80 de potencia y 12 de coraza recibe un ataque de 20 puntos de fuerza, puede parar solamente 12 con la coraza, los otros 8 se descuentan de la potencia. La nave debe quedar con 72 de potencia y 0 de coraza. Si la Enterprise no tiene nada de coraza al momento de recibir el ataque, entonces todos los puntos de fuerza del ataque se descuentan de la potencia. La potencia y la coraza tienen que mantenerse en los rangos indicados, p.ej. si la Enterprise tien 16 puntos de coraza y se encuentra con un escudo, entonces queda en 20 puntos de coraza, no en 26. Tampoco puede quedar negativa la potencia, a lo sumo queda en 0. La Enterprise nace con 50 de potencia y 5 de coraza. Implementar este metodo constructor de la Enterprise, que tiene que entender los siguientes mensajes: 12.1 potencia() 12.2 coraza() 12.3 encontrarPilaAtomica() 12.4 encontrarEscudo() 12.5 recibirAtaque(puntos). --P.ej. sobre un REPL recién lanzado, después de esta secuencia enterprise.encontrarPilaAtomica() enterprise.recibirAtaque(14) enterprise.encontrarEscudo() la potencia de la Enterprise debe ser 66, y su coraza debe ser 10.


```js

function Enterprise(nivelPotencia, nivelCoraza) {

    this.nivelPotencia = nivelPotencia;
    this.nivelCoraza = nivelCoraza;

    this.potencia = function (potenciaInicial) {

        this.nivelPotencia = potenciaInicial;

    }, this.coraza = function (corazaInicial) {

        this.nivelCoraza = corazaInicial;

    }, this.encontrarPilaAtomica = function () {

        const pila = 25;

        if (this.nivelPotencia + pila > 100 || !this.nivelPotencia == 100) {

            let resto = 0;
            resto = 100 - this.nivelPotencia;
            this.nivelPotencia = this.nivelPotencia + resto;

        } else {
            this.nivelPotencia = this.nivelPotencia + pila;

        }


    }, this.encontrarEscudo = function () {

        const escudo = 10;

        if (this.nivelCoraza + escudo > 20 || !this.nivelCoraza == 20) {

            let resto = 0;
            resto = 20 - this.nivelCoraza;
            this.nivelCoraza = this.nivelCoraza + resto;
            escudo = 0;

        } else if (this.nivelCoraza == 20) {

            return;

        } else if (this.nivelCoraza >= 0 || this.nivelCoraza <= 20) {

            this.nivelCoraza = this.nivelCoraza + escudo;
        }

    }, this.recibirAtaque = function (ataquePuntos) {

        if (ataquePuntos > (this.nivelCoraza + this.nivelPotencia)) {

            this.nivelPotencia = 0;
            this.nivelCoraza = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos > 0) {

            this.nivelCoraza = this.nivelCoraza - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos >= 0) {

            this.nivelPotencia = this.nivelPotencia - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos <= 0) {

            this.nivelPotencia = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos <= 0 && !ataquePuntos < this.nivelPotencia) {

            let resto = 0;
            let actual = 0;
            resto = ataquePuntos - this.nivelCoraza;
            actual = ataquePuntos - resto;
            this.nivelCoraza = this.nivelCoraza - actual;
            this.nivelPotencia = this.nivelPotencia - resto;

        }
    }, this.mostrarDatosEnterprise = function(enterprise) {

        return `Enterprise =  Nivel potencia: ${enterprise.nivelPotencia}, Nivel coraza:  ${enterprise.nivelCoraza}`
    }

}

let enterprise = new Enterprise();

enterprise.potencia(50);
enterprise.coraza(5);

enterprise.encontrarPilaAtomica();
enterprise.recibirAtaque(14);
enterprise.encontrarEscudo();
enterprise.mostrarDatosEnterprise(enterprise);

```

![Alt text](./img/23.jpg?raw=true "Title")


### EJERCICIO 13 -Agregar al metodo constructor de la Enterprise del ejercicio 12, la capacidad de entender estos mensajes. fortalezaDefensiva(), que es el máximo nivel de ataque que puede resistir, o sea, coraza más potencia. necesitaFortalecerse(), tiene que ser true si su coraza es 0 y su potencia es menos de 20. fortalezaOfensiva(), que corresponde a cuántos puntos de fuerza tendría un ataque de la Enterprise. Se calcula así: si tiene menos de 20 puntos de potencia entonces es 0, si no es (puntos de potencia - 20) / 2.

```js

function Enterprise(nivelPotencia, nivelCoraza) {

    this.nivelPotencia = nivelPotencia;
    this.nivelCoraza = nivelCoraza;

    this.potencia = function (potenciaInicial) {

        this.nivelPotencia = potenciaInicial;

    }, this.coraza = function (corazaInicial) {

        this.nivelCoraza = corazaInicial;

    }, this.encontrarPilaAtomica = function () {

        const pila = 25;

        if (this.nivelPotencia + pila > 100 || !this.nivelPotencia == 100) {

            var resto = 0;
            resto = 100 - this.nivelPotencia;
            this.nivelPotencia = this.nivelPotencia + resto;

        } else {
            this.nivelPotencia = this.nivelPotencia + pila;

        }


    }, this.encontrarEscudo = function () {

        const escudo = 10;

        if (this.nivelCoraza + escudo > 20 || !this.nivelCoraza == 20) {

            let resto = 0;
            resto = 20 - this.nivelCoraza;
            this.nivelCoraza = this.nivelCoraza + resto;
            escudo = 0;

        } else if (this.nivelCoraza == 20) {

            return;

        } else if (this.nivelCoraza >= 0 || this.nivelCoraza <= 20) {

            this.nivelCoraza = this.nivelCoraza + escudo;
        }

    }, this.recibirAtaque = function (ataquePuntos) {

        if (ataquePuntos > (this.nivelCoraza + this.nivelPotencia)) {

            this.nivelPotencia = 0;
            this.nivelCoraza = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos > 0) {

            this.nivelCoraza = this.nivelCoraza - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos >= 0) {

            this.nivelPotencia = this.nivelPotencia - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos <= 0) {

            this.nivelPotencia = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos <= 0 && !ataquePuntos < this.nivelPotencia) {

            let resto = 0;
            let actual = 0;
            resto = ataquePuntos - this.nivelCoraza;
            actual = ataquePuntos - resto;
            this.nivelCoraza = this.nivelCoraza - actual;
            this.nivelPotencia = this.nivelPotencia - resto;

        }
    }, this.fortalezaDefensiva = function () {



        let fortalezaDef = this.nivelCoraza + this.nivelPotencia;
        return console.log('Fortaleza defentiva total: ' + fortalezaDef);

    }, this.necesitaFortalecerse = function () {

        if (this.nivelCoraza == 0 && this.nivelPotencia < 20) {

            console.log('Necesita fortalecer')
            return true;
        } else {
            console.log('No necesita fortalecer')
            return true;
        }


    }, this.fortalezaOfensiva = function () {


        if (this.nivelPotencia < 20) {

            console.log('La potencia esta por debajo de 20, los puntos de fuerza en ataque son de 0')

        } else {

            let puntosPotencia = (this.nivelPotencia - 20) / 2;
            console.log('los puntos de fuerza en ataque son de: ' + puntosPotencia)
        }
    },this.mostrarDatosEnterprise = function(enterprise) {

        return `Enterprise =  Nivel potencia: ${enterprise.nivelPotencia}, Nivel coraza:  ${enterprise.nivelCoraza}`
    }

}

let enterprise = new Enterprise();

enterprise.potencia(50);
enterprise.coraza(5);
enterprise.mostrarDatosEnterprise(enterprise);

enterprise.fortalezaDefensiva();
enterprise.necesitaFortalecerse();
enterprise.fortalezaOfensiva();

```
![Alt text](./img/24.jpg?raw=true "Title")


### EJERCICIO 14 -Un taller de diseño de autos quiere estudiar un nuevo prototipo. Para eso, nos piden hacer un metodo constructor concentrado en las características del motor. El prototipo de motor tiene 5 cambios (de primera a quinta), y soporta hasta 5000 RPM. La velocidad del auto se calcula así: (rpm / 100) * (0.5 + (cambio / 2)). P.ej. en tercera a 2000 rpm, la velocidad es 20 * (0.5 + 1.5) = 40. También nos interesa controlar el consumo. Se parte de una base de 0.05 litros por kilómetro. A este valor se le aplican los siguientes ajustes: Si el motor está a más de 3000 rpm, entonces se multiplica por (rpm - 2500) / 500. P.ej., a 3500 rpm hay que multiplicar por 2, a 4000 rpm por 3, etc. Si el motor está en primera, entonces se multiplica por 3. Si el motor está en segunda, entonces se multiplica por 2. Los efectos por revoluciones y por cambio se acumulan. P.ej. si el motor está en primera y a 5000 rpm, entonces el consumo es 0.05 * 5 * 3 = 0.75 litros/km. El metodo constructor debe entender estos mensajes: arrancar(), se pone en primera con 500 rpm. subirCambio() bajarCambio() subirRPM(cuantos) bajarRPM(cuantos) velocidad() consumoActualPorKm().


```js
function Motor(cambio, rpm) {

    this.cambio = cambio;
    this.rpm = rpm;

    this.arrancar = function () {

        this.cambio = 1;
        this.rpm = 500;

    }, this.subirCambio = function () {

        if (this.cambio == 5) {

            console.log('No es posible subir de cambio')

        } else {

            this.cambio++;
        }


    }, this.bajarCambio = function () {

        if (this.cambio == 1) {

            console.log('No es posible bajar de cambio')

        } else {

            this.cambio--;
        }


    }, this.subirRPM = function (cuanto) {

        if (this.rpm > 5000) {

            console.log('No es posible subir de RPM')

        } else {

            this.rpm = this.rpm + cuanto;
        }

    }, this.bajarRPM = function (cuanto) {

        if (this.rpm == 0 && this.rpm - cuanto < 0) {

            console.log('No es posible bajar de RPM')

        } else {

            this.rpm = this.rpm - cuanto;
        }

    }, this.velocidad = function () {


        const vel = (this.rpm / 100) * (0.5 + (this.cambio / 2))
        console.log('La veclocidad es de: ' + vel + ' km/h')
        return vel;

    }, this.consumoActualKm = function () {

        let consumo = 0;

        if (this.cambio == 1 && this.rpm > 3000) {

            consumo = 0.05 * ((this.rpm - 2500) / 500);
            consumo = consumo * 3;
            return console.log(consumo + ' litros/km.')

        } else if (this.cambio == 2 && this.rpm > 3000) {

            consumo = 0.05 * ((this.rpm - 2500) / 500);
            consumo = consumo * 2;
            return console.log(consumo + ' litros/km.')

        } else if (this.cambio == 1) {

            consumo = 0.05 * 3;
            return console.log(consumo + ' litros/km.')

        } else if (this.cambio == 2) {

            consumo = 0.05 * 2;
            return console.log(consumo + ' litros/km.')

        }


    },this.mostrarDatosMotor = function(motor) {

        return `Motor =  Cambio actual: ${motor.cambio}, RPM actual:  ${motor.rpm}`
    }

}

let motor = new Motor();

motor.arrancar();
motor.subirCambio();
motor.subirCambio();
motor.mostrarDatosMotor(motor);
motor.subirRPM(1500);
motor.velocidad();

motor.bajarCambio();
motor.consumoActualKm();
motor.mostrarDatosMotor(motor);

```
![Alt text](./img/25.jpg?raw=true "Title")

