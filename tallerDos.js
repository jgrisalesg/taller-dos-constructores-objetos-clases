
//////////////  EJERCICIO 1 //////////////////////////// 

function persona(nombre, edad, cedula) {

    this.nombre = nombre;
    this.edad = edad;
    this.cedula = cedula;

    this.mostrarDatos = function () {

        return `Persona =  Nombre: ${this.nombre}, Edad:  ${this.edad},  Cedula:  ${this.cedula}`

    }, this.mayor = function (persona) {

        if (persona.edad >= 18) {

            console.log(persona.nombre + ' es mayor de edad')
            return true
        } else {

            console.log(persona.nombre + ' no es mayor de edad')
            return false
        }
    }

}

let personaUno = new persona("Jhonathan", 17, 1060650078);
personaUno.mostrarDatos();
personaUno.mayor(personaUno);


//////////////  EJERCICIO 2 ////////////////////////////  PREGUNTA

function cuenta() {

    this.nombre = '';
    this.cantidad = '';

    this.crear = function () {

        while (this.nombre == '' || this.cantidad == '' || this.nombre == null || this.cantidad == null) {

            this.nombre = prompt('Ingrese Nombre Titular');
            this.cantidad = prompt('Ingrese cantidad inicial')

        }

        this.cantidad = parseFloat(this.cantidad);

    }, this.mostrar = function () {

        return console.log(' Nombre del titular: ' + this.nombre + " , Cantidad en cuenta: " + this.cantidad);

    }, this.ingresarCantidad = function (cantidadCuenta) {

        if (cantidadCuenta >= 0) {

            this.cantidad = this.cantidad + cantidadCuenta;

        } else if (cantidadCuenta < 0) {

            this.cantidad = this.cantidad;

        }


    }, this.retirarCantidad = function (retiro) {

        if (retiro >= 0) {

            this.cantidad = this.cantidad - retiro;
        }

    }

}


let cuentaUno = new cuenta();
cuentaUno.crear();
cuentaUno.mostrar();
cuentaUno.ingresarCantidad(2500);
cuentaUno.mostrar();
cuentaUno.retirarCantidad(1500);
cuentaUno.mostrar();



//////////////  EJERCICIO 3 //////////////////////////// 

function formulas() {

    this.sumar = function (enteroUno, enteroDos) {

        return `${enteroUno + enteroDos}`

    }, this.fibonacci = function (cantidad) {

        let numeros = [0, 1];

        for (let i = 2; i < cantidad; i++) {

            numeros[i] = numeros[i - 2] + numeros[i - 1];
        }
        return numeros;

    }, this.operacionModulo = function (cantidad) {

        let elemento = [0];
        let contador = 0; 

        for (let index = 1; index <= cantidad; index++) {


            if (index % 2 == 0) {      

                  elemento[contador]= index ;
                  contador++;
                  
            }

        }

        console.log('Los siguientes numeros al dividirlos por 2 dan residuo 0')
        return elemento



    }, this.numerosPrimos = function (primos) {

        for (let x = 2; x <= primos; x++) {

            let primo = true;
            const div = 2;

            while (primo && div < x) {

                if (x % div == 0) {

                    primo = false

                } else {

                    div = div + 1;
                }
            }
            if (primo == true)

                console.log(x)
        }

    }
}

let formula = new formulas();
formula.sumar(4, 7);
formula.fibonacci(10);
formula.numerosPrimos(20);
formula.operacionModulo(10);

//////////////  EJERCICIO 4 //////////////////////////// 

function personaIMC(nombre, edad, DNI, sexo, peso, altura) {

    this.nombre = nombre;
    this.edad = edad;
    this.DNI = DNI;
    this.sexo = sexo;
    this.peso = peso;
    this.altura = altura;

    this.pesoIdeal = function (peso, altura) {


        let IMC = (peso / (altura ** 2));

        if (IMC < 20) {

            return -1;

        } else if (IMC >= 20 && IMC <= 25) {

            return 0;

        } else if (IMC > 25) {

            return 1;
        }


    }, this.mayorEdad = function (persona) {

        if (persona.edad >= 18) {

            console.log(persona.nombre + ' es mayor de edad')
            return true

        } else {

            console.log(persona.nombre + ' no es mayor de edad')
            return false
        }

    }, this.comprobar = function (sexo) {

        if (sexo == 'M' || sexo == 'H') {

            console.log('Sexo correcto')
            return true

        } else {

            console.log('Sexo no correcto!!! se reestablece por el predeterminado H')
            this.sexo = 'H'
        }
    }
}

let personaDos = new personaIMC("Camilo", 45, 1060650078, 'M', 102, 1.82);
personaDos.pesoIdeal();
personaDos.mayorEdad();
personaDos.comprobar();


//////////////  EJERCICIO 5 //////////////////////////// 

function contraseña(longitud, contraseña) {

    this.longitud = longitud;
    this.contraseña = contraseña;

    this.esFuerte = function (contraseñaComprobar) {


        var cuentanumeros = 0;
        var cuentaminusculas = 0;
        var cuentamayusculas = 0;

        //Recorriend contraseña y verificando el tipo de dato 
        for (let i = 0; i < contraseñaComprobar.contraseña.length; i++) {

            if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toLowerCase() && isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentaminusculas = cuentaminusculas + 1;

            } else if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toUpperCase() && isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentamayusculas = cuentamayusculas + 1;

            } else if (!isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentanumeros = cuentanumeros + 1;
            }
        }

        //Si la constraseña tiene mas de 5 numeros, mas de 1 minuscula y mas de 2 mayusculas
        if (cuentanumeros >= 5 && cuentaminusculas >= 1 && cuentamayusculas >= 2) {
            console.log('Es fuerte')
            return true;
        } else {

            console.log('No es fuerte')
            return false;
        }
    }, this.generarPassword = function (longitud) {

        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;

        for (let i = 0; i < longitud; i++) {

            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        this.contraseña = result;
        return result;

    }, this.seguridadPaswword = function (contraseñaComprobar) {

        let cuentanumeros = 0;
        let cuentaminusculas = 0;
        let cuentamayusculas = 0;
        let cunentaespeciales = 0;
        let letrasTotal = 0;


        const iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";

        for (var i = 0; i < contraseñaComprobar.contraseña.length; i++) {

            if (iChars.indexOf(contraseñaComprobar.contraseña.charAt(i)) != -1) {

                cunentaespeciales = cunentaespeciales + 1;

            }
        }


        //Recorriend contraseña y verificando el tipo de dato 
        for (var i = 0; i < contraseñaComprobar.contraseña.length; i++) {

            if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toLowerCase()
                && isNaN(contraseñaComprobar.contraseña.charAt(i)) && iChars.indexOf(contraseñaComprobar.contraseña.charAt(i)) == -1) {
                cuentaminusculas = cuentaminusculas + 1;

            } else if (contraseñaComprobar.contraseña.charAt(i) == contraseñaComprobar.contraseña.charAt(i).toUpperCase()
                && isNaN(contraseñaComprobar.contraseña.charAt(i)) && iChars.indexOf(contraseñaComprobar.contraseña.charAt(i)) == -1) {
                cuentamayusculas = cuentamayusculas + 1;

            } else if (!isNaN(contraseñaComprobar.contraseña.charAt(i))) {
                cuentanumeros = cuentanumeros + 1;
            }
        }

        letrasTotal = cuentamayusculas + cuentaminusculas;


        //DEBIL
        if (cuentanumeros + letrasTotal <= 6) {

            return console.log('Contraseña Debil')
            //MEDIA
        } else if (cuentanumeros + letrasTotal >= 7 && cuentanumeros + letrasTotal <= 10) {

            return console.log('Contraseña Media')
            //ALTA  
        } else if (cunentaespeciales + letrasTotal + cuentanumeros >= 11 && cuentanumeros + letrasTotal + cunentaespeciales <= 20 && cunentaespeciales > 0) {

            return console.log('Contraseña Alta')

        } else if (cunentaespeciales + letrasTotal + cuentanumeros >= 11 && cuentanumeros + letrasTotal + cunentaespeciales <= 20 && cunentaespeciales == 0) {

            return console.log('La contraseña es Alta pero no contiene caracteres especiales')

        } else if (cunentaespeciales + letrasTotal + cuentanumeros > 20 || letrasTotal + cuentanumeros + cunentaespeciales > 20) {

            return console.log('Contraseña excede el numero de evaluacion soportada')


        }




    }

}

let contraseñaUno = new contraseña(12, '12345ABCab');
let contraseñaDos = new contraseña(12, '');

contraseñaUno.esFuerte(contraseñaUno);
contraseñaDos.generarPassword(contraseñaDos.longitud);
contraseñaDos.seguridadPaswword(contraseñaDos);




//////////////  EJERCICIO 6 //////////////////////////// 

function Contador(ultimoComando) {

    this.reset = function () {
        this.numero = 0;

    },
        this.inc = function () {
            this.numero++

        },
        this.dec = function () {
            this.numero--

        },
        this.valorActual = function (valor) {


            if (valor != null) {
                this.numero = valor;

            } else {
                return this.numero;

            }
        }
}

//SE CREA EL OBJETO Y SE HACEN LOS PASOS (RESULTADO FINAL ES 12)

let contador = new Contador();
contador.reset();
contador.valorActual(10);
contador.inc();
contador.inc();
contador.dec();
contador.inc();
console.log(contador.valorActual());

//////////////  EJERCICIO 7 //////////////////////////// 

function Contador(ultimoComando) {

    this.ultimoComan = ultimoComando;

    this.reset = function () {
        this.numero = 0;
        this.ultimoComan = 'reset'
    },
        this.inc = function () {
            this.numero++
            this.ultimoComan = 'Incremento'
        },
        this.dec = function () {
            this.numero--
            this.ultimoComan = 'Decremento'
        },
        this.valorActual = function (valor) {

            if (valor != null) {
                this.numero = valor;
                this.ultimoComan = 'Actualizacion'
            } else {
                return this.numero;

            }
        }, this.ultimoComando = function () {

            return this.ultimoComan;
        }
}

contador.ultimoComando()



//////////////  EJERCICIO 8 //////////////////////////// 


function Chimuela(energiaActual) {

    this.energiaActual = energiaActual;

    this.comer = function (gramos) {

        this.energiaActual = this.energiaActual + (gramos * 4);

    }, this.volar = function (kilometros) {

        this.energiaActual = this.energiaActual - (kilometros);
        this.energiaActual = this.energiaActual - 10;

    }, this.energia = function () {

        return this.energiaActual;

    }
}

let chimuela = new Chimuela(0);

chimuela.comer(100)
chimuela.volar(10);
chimuela.volar(20);
console.log ('la energia actual es de: ' + chimuela.energia() + ' joules' )


//////////////  EJERCICIO 9 //////////////////////////// 

function Chimuela(energiaActual) {

    this.energiaActual = energiaActual;

    this.comer = function (gramos) {

        this.energiaActual = this.energiaActual + (gramos * 4);

    }, this.volar = function (kilometros) {

        this.energiaActual = this.energiaActual - (kilometros);
        this.energiaActual = this.energiaActual - 10;

    }, this.energia = function () {

        return this.energiaActual;

    }, this.estadoEnergia = function (dragon) {

        if (dragon.energiaActual <= 50) {

            return console.log('Chimuelo esta debil')

        } else if (dragon.energiaActual >= 50 && dragon.energiaActual <= 1000) {

            return console.log('Chimuelo esta Feliz')
        }
    }, this.cuantoQuiereVolar = function () {

        var km = (this.energiaActual / 5)

        if (this.energiaActual < 300) {

            return console.log('chimuela quiere volar ' + km)

        } else if (this.energiaActual >= 300 && this.energiaActual <= 400 && this.energiaActual % 20 != 0) {

            km = km + 10;

            return console.log('chimuela quiere volar ' + km)

        } else if (this.energiaActual >= 300 && this.energiaActual <= 400 && this.energiaActual % 20 == 0) {

            km = km + 10 + 15

            return console.log('chimuela quiere vola ' + km)
        }

    }

}

let chimuela = new Chimuela(0);


chimuela.comer(85)
chimuela.volar(10);
chimuela.volar(20);
console.log ('la energia actual es de: ' + chimuela.energia() + ' joules' )

chimuela.estadoEnergia(chimuela);
chimuela.cuantoQuiereVolar();


//////////////  EJERCICIO 10 //////////////////////////// 

function Calculadora() {

    this.cargarNumero = function (numero) {

        this.cargar = numero;

    }, this.sumar = function (numero) {

        this.cargar = this.cargar + numero;

    }, this.restar = function (numero) {

        this.cargar = this.cargar - numero;

    }, this.multiplicar = function (numero) {

        this.cargar = this.cargar * numero;

    }, this.valorActuloperacion = function () {

        return this.cargar;

    }
}

let calculadora = new Calculadora();

calculadora.cargarNumero(0)
calculadora.sumar(4)
calculadora.multiplicar(5)
calculadora.restar(8)
calculadora.multiplicar(2)
calculadora.valorActuloperacion()


//////////////  EJERCICIO 11 //////////////////////////// 


function Libro(tituloLibro, autor, numeroEjemplares, numeroEjemplaresPrestado) {

    this.tituloLibro = tituloLibro;
    this.autor = autor;
    this.numeroEjemplares = numeroEjemplares;
    this.numeroEjemplaresPrestado = numeroEjemplaresPrestado;

    this.prestamo = function (Libro) {

        if (Libro.numeroEjemplaresPrestado < Libro.numeroEjemplares) {

            Libro.numeroEjemplaresPrestado++;
            console.log('Se presto el libro: ' + Libro.tituloLibro)
            return true;

        } else if (Libro.numeroEjemplaresPrestado == Libro.numeroEjemplares) {

            console.log('No hay ejemplares disponibles del libro:  ' + Libro.tituloLibro)
            return false;

        }
    }, this.devolucion = function (Libro) {

        if (Libro.numeroEjemplaresPrestado <= Libro.numeroEjemplares && Libro.numeroEjemplaresPrestado != 0) {

            Libro.numeroEjemplaresPrestado--;
            console.log('Se devolvio el librro : ' + Libro.tituloLibro)
            return true;

        } else if (Libro.numeroEjemplaresPrestado == 0) {

            console.log('El libro ' + Libro.tituloLibro + ' no esta prestado')
            return false;


        }

    }, this.toString = function () {

        return console.log('Titulo libro: ' + this.tituloLibro + ', Autor: ' + this.autor +
            ', Numero ejemplares: ' + this.numeroEjemplares + ', Numero ejemeplares prestado: ' + this.numeroEjemplaresPrestado)

    }
}




let libroUno = new Libro('Principito', 'Antonie', 5, 0);
let libroDos = new Libro('Nacho lee', 'Jorge Luis', 7, 0);
let libroTres = new Libro('Coquito clasico', 'Adrian Duf.', 3, 0);

libroUno.toString();

libroUno.prestamo(libroUno);
libroUno.prestamo(libroUno);
libroUno.prestamo(libroUno);
libroUno.prestamo(libroUno);

libroUno.devolucion(libroUno);
libroUno.devolucion(libroUno);
libroUno.devolucion(libroUno);
libroUno.devolucion(libroUno);


//////////////  EJERCICIO 12 //////////////////////////// 

function Enterprise(nivelPotencia, nivelCoraza) {

    this.nivelPotencia = nivelPotencia;
    this.nivelCoraza = nivelCoraza;

    this.potencia = function (potenciaInicial) {

        this.nivelPotencia = potenciaInicial;

    }, this.coraza = function (corazaInicial) {

        this.nivelCoraza = corazaInicial;

    }, this.encontrarPilaAtomica = function () {

        const pila = 25;

        if (this.nivelPotencia + pila > 100 || !this.nivelPotencia == 100) {

            let resto = 0;
            resto = 100 - this.nivelPotencia;
            this.nivelPotencia = this.nivelPotencia + resto;

        } else {
            this.nivelPotencia = this.nivelPotencia + pila;

        }


    }, this.encontrarEscudo = function () {

        const escudo = 10;

        if (this.nivelCoraza + escudo > 20 || !this.nivelCoraza == 20) {

            let resto = 0;
            resto = 20 - this.nivelCoraza;
            this.nivelCoraza = this.nivelCoraza + resto;
            escudo = 0;

        } else if (this.nivelCoraza == 20) {

            return;

        } else if (this.nivelCoraza >= 0 || this.nivelCoraza <= 20) {

            this.nivelCoraza = this.nivelCoraza + escudo;
        }

    }, this.recibirAtaque = function (ataquePuntos) {

        if (ataquePuntos > (this.nivelCoraza + this.nivelPotencia)) {

            this.nivelPotencia = 0;
            this.nivelCoraza = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos > 0) {

            this.nivelCoraza = this.nivelCoraza - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos >= 0) {

            this.nivelPotencia = this.nivelPotencia - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos <= 0) {

            this.nivelPotencia = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos <= 0 && !ataquePuntos < this.nivelPotencia) {

            let resto = 0;
            let actual = 0;
            resto = ataquePuntos - this.nivelCoraza;
            actual = ataquePuntos - resto;
            this.nivelCoraza = this.nivelCoraza - actual;
            this.nivelPotencia = this.nivelPotencia - resto;

        }
    }, this.mostrarDatosEnterprise = function(enterprise) {

        return `Enterprise =  Nivel potencia: ${enterprise.nivelPotencia}, Nivel coraza:  ${enterprise.nivelCoraza}`
    }

}

let enterprise = new Enterprise();

enterprise.potencia(50);
enterprise.coraza(5);

enterprise.encontrarPilaAtomica();
enterprise.recibirAtaque(14);
enterprise.encontrarEscudo();
enterprise.mostrarDatosEnterprise(enterprise);


//////////////  EJERCICIO 13 //////////////////////////// 

function Enterprise(nivelPotencia, nivelCoraza) {

    this.nivelPotencia = nivelPotencia;
    this.nivelCoraza = nivelCoraza;

    this.potencia = function (potenciaInicial) {

        this.nivelPotencia = potenciaInicial;

    }, this.coraza = function (corazaInicial) {

        this.nivelCoraza = corazaInicial;

    }, this.encontrarPilaAtomica = function () {

        const pila = 25;

        if (this.nivelPotencia + pila > 100 || !this.nivelPotencia == 100) {

            var resto = 0;
            resto = 100 - this.nivelPotencia;
            this.nivelPotencia = this.nivelPotencia + resto;

        } else {
            this.nivelPotencia = this.nivelPotencia + pila;

        }


    }, this.encontrarEscudo = function () {

        const escudo = 10;

        if (this.nivelCoraza + escudo > 20 || !this.nivelCoraza == 20) {

            let resto = 0;
            resto = 20 - this.nivelCoraza;
            this.nivelCoraza = this.nivelCoraza + resto;
            escudo = 0;

        } else if (this.nivelCoraza == 20) {

            return;

        } else if (this.nivelCoraza >= 0 || this.nivelCoraza <= 20) {

            this.nivelCoraza = this.nivelCoraza + escudo;
        }

    }, this.recibirAtaque = function (ataquePuntos) {

        if (ataquePuntos > (this.nivelCoraza + this.nivelPotencia)) {

            this.nivelPotencia = 0;
            this.nivelCoraza = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos > 0) {

            this.nivelCoraza = this.nivelCoraza - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos >= 0) {

            this.nivelPotencia = this.nivelPotencia - ataquePuntos;

        } else if (this.nivelCoraza == 0 && this.nivelPotencia - ataquePuntos <= 0) {

            this.nivelPotencia = 0;
            console.log('GAME OVER')

        } else if (this.nivelCoraza - ataquePuntos <= 0 && !ataquePuntos < this.nivelPotencia) {

            let resto = 0;
            let actual = 0;
            resto = ataquePuntos - this.nivelCoraza;
            actual = ataquePuntos - resto;
            this.nivelCoraza = this.nivelCoraza - actual;
            this.nivelPotencia = this.nivelPotencia - resto;

        }
    }, this.fortalezaDefensiva = function () {



        let fortalezaDef = this.nivelCoraza + this.nivelPotencia;
        return console.log('Fortaleza defentiva total: ' + fortalezaDef);

    }, this.necesitaFortalecerse = function () {

        if (this.nivelCoraza == 0 && this.nivelPotencia < 20) {

            console.log('Necesita fortalecer')
            return true;
        } else {
            console.log('No necesita fortalecer')
            return true;
        }


    }, this.fortalezaOfensiva = function () {


        if (this.nivelPotencia < 20) {

            console.log('La potencia esta por debajo de 20, los puntos de fuerza en ataque son de 0')

        } else {

            let puntosPotencia = (this.nivelPotencia - 20) / 2;
            console.log('los puntos de fuerza en ataque son de: ' + puntosPotencia)
        }
    },this.mostrarDatosEnterprise = function(enterprise) {

        return `Enterprise =  Nivel potencia: ${enterprise.nivelPotencia}, Nivel coraza:  ${enterprise.nivelCoraza}`
    }

}

let enterprise = new Enterprise();

enterprise.potencia(50);
enterprise.coraza(5);
enterprise.mostrarDatosEnterprise(enterprise);

enterprise.fortalezaDefensiva();
enterprise.necesitaFortalecerse();
enterprise.fortalezaOfensiva();

//////////////  EJERCICIO 14 //////////////////////////// 

function Motor(cambio, rpm) {

    this.cambio = cambio;
    this.rpm = rpm;

    this.arrancar = function () {

        this.cambio = 1;
        this.rpm = 500;

    }, this.subirCambio = function () {

        if (this.cambio == 5) {

            console.log('No es posible subir de cambio')

        } else {

            this.cambio++;
        }


    }, this.bajarCambio = function () {

        if (this.cambio == 1) {

            console.log('No es posible bajar de cambio')

        } else {

            this.cambio--;
        }


    }, this.subirRPM = function (cuanto) {

        if (this.rpm > 5000) {

            console.log('No es posible subir de RPM')

        } else {

            this.rpm = this.rpm + cuanto;
        }

    }, this.bajarRPM = function (cuanto) {

        if (this.rpm == 0 && this.rpm - cuanto < 0) {

            console.log('No es posible bajar de RPM')

        } else {

            this.rpm = this.rpm - cuanto;
        }

    }, this.velocidad = function () {


        const vel = (this.rpm / 100) * (0.5 + (this.cambio / 2))
        console.log('La veclocidad es de: ' + vel + ' km/h')
        return vel;

    }, this.consumoActualKm = function () {

        let consumo = 0;

        if (this.cambio == 1 && this.rpm > 3000) {

            consumo = 0.05 * ((this.rpm - 2500) / 500);
            consumo = consumo * 3;
            return console.log(consumo + ' litros/km.')

        } else if (this.cambio == 2 && this.rpm > 3000) {

            consumo = 0.05 * ((this.rpm - 2500) / 500);
            consumo = consumo * 2;
            return console.log(consumo + ' litros/km.')

        } else if (this.cambio == 1) {

            consumo = 0.05 * 3;
            return console.log(consumo + ' litros/km.')

        } else if (this.cambio == 2) {

            consumo = 0.05 * 2;
            return console.log(consumo + ' litros/km.')

        }


    },this.mostrarDatosMotor = function(motor) {

        return `Motor =  Cambio actual: ${motor.cambio}, RPM actual:  ${motor.rpm}`
    }

}

let motor = new Motor();

motor.arrancar();
motor.subirCambio();
motor.subirCambio();
motor.mostrarDatosMotor(motor);
motor.subirRPM(1500);
motor.velocidad();

motor.bajarCambio();
motor.consumoActualKm();
motor.mostrarDatosMotor(motor);
